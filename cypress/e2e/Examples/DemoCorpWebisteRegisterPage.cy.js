/// <reference types="cypress" />
import { visitUrl } from "../PageObject/Registerpage.js";
import {
  verifyRegistrationFormWithValidDetails,
  verifyRegistrationFormWithInvalidEmail,
  verifyRegistrationFormWithInvalidPassword,
} from "../PageObject/Registration.js";

describe("DemoCorp Registration Test Suite", function () {
  beforeEach(() => {
    visitUrl();
  });
  it("Registration with valid credentials", function () {
    verifyRegistrationFormWithValidDetails();
  });
  it("Registration with invalid email", function () {
    verifyRegistrationFormWithInvalidEmail();
  });
  it("Registration with invalid password", function () {
    verifyRegistrationFormWithInvalidPassword();
  });
});
