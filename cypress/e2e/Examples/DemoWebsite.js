// <reference types="Cypress"/>


describe('Testing Website Form', function()
{
    beforeEach(() => {
        cy.visit("https://demo.nopcommerce.com/")
    })


    it('Testing',function() {
        cy.get(".ico-register").click({force: true})
        cy.wait(2000)
        cy.title("nopCommerce demo store. Register").should('eq','nopCommerce demo store. Register')
        cy.get("#gender-male").should('be.visible').should('not.be.checked').click() ///not checked chekbox
        cy.get("#FirstName").type('Shalini')
        cy.wait(2000)
        cy.get("#LastName").type('Yadav')

        cy.wait(5000)
        cy.get("[name='DateOfBirthDay']").select('28').should('have.value', '28') ///static dropdown
        cy.get("[name='DateOfBirthMonth']").select('August')  ///static dropdown
        cy.get('select[name="DateOfBirthYear"]').select('1994').should('have.value', '1994')
    
        cy.get("#Email").type('shalini.yadav@fiftyfivetech.io')
        cy.get("#Company").type('Jaipur').should('have.value','Jaipur')
   
        cy.get("#Password").type('1234567890')
        cy.get("#ConfirmPassword").type('1234567890')
    
        cy.wait(5000)
        cy.get("#register-button").click()
        cy.url("demo.nopcommerce.com").should('include','demo') ///URL validation
        //cy.get("#Newsletter").check().should('be.checked')  ///checkbox
    
        

        
    })
})
