/// <reference types="cypress" />



import { navigate, verifyDashboard,getLoginCredentials,verifyInvalidCredentialsErrorMessage } from "../PageObject/fake";

describe("Cypress POM Test Suite", function () {
  beforeEach(() => {
    navigate();
    cy.fixture("Dummywebsite").then(function (data) {
      this.data = data;
    });
  });
  it("Login with valid credentials", function () {
    getLoginCredentials(this.data.username,this.data.password);
    verifyDashboard(); 
  });
  it("Login with invalid username", function () {
    getLoginCredentials(this.data.invalidusername,this.data.password);
    verifyInvalidCredentialsErrorMessage();
  });
  it("Login with invalid password", function () {
    getLoginCredentials(this.data.username,this.data.invalidpassword);
    verifyInvalidCredentialsErrorMessage();
    });
});
