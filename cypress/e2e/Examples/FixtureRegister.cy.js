// <reference types="Cypress"/>



describe('Demo Corp Website',function() {
    before(function() {
        cy.fixture('DemoCorpWebsiteFixtureFile').then(function(data)  
        {
            this.data=data
        })

    })
        
    

    it('Registration Feature',function() {
        cy.visit('/')

        
        
       
        
        cy.get('#FirstName').type(this.data.FirstName)
        cy.get('#LastName').type(this.data.LastName)
        cy.get('#Email').type(this.data.email)
        cy.get('#Company').type(this.data.company)
        cy.get('#Password').type(this.data.password)
        cy.get('#ConfirmPassword').type(this.data.confirmpassword)
        
        cy.get("form > .buttons > .button-1").click()
        cy.url("demo.nopcommerce.com").should('include','demo')

    })

})