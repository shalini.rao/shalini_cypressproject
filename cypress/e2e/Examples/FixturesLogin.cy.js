// <reference types="Cypress"/>



describe('Demo Corp Website',function() {
    before(function() {
        cy.fixture('example').then(function(data)  
        {
            this.data=data
        })

    })
        
    

    it('LOGIN Feature',function() {
        cy.visit("https://demo.nopcommerce.com/login?returnUrl=%2F")
        cy.title("nopCommerce demo store. Login").should('eq','nopCommerce demo store. Login')
       
        cy.get('#Email').type(this.data.email)
        cy.get('#Password').type(this.data.password)
        
        cy.get("form > .buttons > .button-1").click()
        cy.url("demo.nopcommerce.com").should('include','demo')

    })

})