/// <reference types="cypress" />

import { navigate,getUsername,getPassword,clickSubmit, verifyDashboard,verifyInvalidCredentialsErrorMessage } from "../PageObject/FunctionRegister";

describe("Cypress POM Test Suite", function () {
  beforeEach(() => {
    navigate();
    cy.fixture("Dummywebsite").then(function (data) {
      this.data = data;
    });
  });
  it("Login with valid credentials", function () {
    getUsername(this.data.username);
    getPassword(this.data.password);
    clickSubmit();
    verifyDashboard();
    
  });
  it("Login with invalid username", function () {
    getUsername(this.data.invalidusername);
    getPassword(this.data.password);
    clickSubmit();
    verifyInvalidCredentialsErrorMessage();
    
    //verifyInvalidEmailErrorMessage();
  });
  it("Login with invalid password", function () {
    getUsername(this.data.username);
    getPassword(this.data.invalidpassword);
    clickSubmit();
    verifyInvalidCredentialsErrorMessage();

    //verifyInvalidPasswordErrorMessage();
  });
});
