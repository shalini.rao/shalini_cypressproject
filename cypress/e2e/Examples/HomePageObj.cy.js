/// <reference types="Cypress"/>
import HomePage from '../PageObject/HomePage'

describe('My Homepage Test Suite',function()
{

before(function(){
    cy.fixture('example').then(function(data)
    {
        this.data=data
    }
)
})
it('My Homepage Test case',function(){

const homePage=new HomePage()
cy.visit('https://rahulshettyacademy.com/angularpractice/')
homePage.getEditBox().type(this.data.name)
homePage.getGender().select(this.data.gender)
homePage.getTwoWayDataBinding().should('have.value',this.data.name)
homePage.getEditBox().should('have.attr','minlength','2')
homePage.getEntrepreneaur().should('be.disabled')
homePage.getShopTab().click()

this.data.productName.forEach(function(element)
{
    cy.selectProduct(element)
});

})
})
