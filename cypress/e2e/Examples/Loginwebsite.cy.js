/// <reference types="Cypress"/>

describe('Demi website Staging login', function()
{
    beforeEach(() => {
        cy.visit("https://demo.nopcommerce.com/")
    it('Valid Staging login',function() {

    })

cy.get(".ico-login").click()
cy.get("#Email").type('shalini.yadav@fiftyfivetech.io').should('have.value','shalini.yadav@fiftyfivetech.io')
cy.get("#Password").type('1234567890').should('have.value','1234567890')

    })
    ///verify login
    it('Valid Staging login negative',function() {


        
        cy.get(".ico-login").click()
        cy.get("#Email").type('abc@ab.com').should('have.value','abc@ab.com')
       cy.get("#Password").type('123456789').should('have.value','123456789')

    })
    ///Verify checkbox
    it('validate check box',function() {


        
        cy.get("#RememberMe").click().should('be.checked')
        //cy.get("#RememberMe").click().should('not.be.checked') to uncheck
        cy.get(".login-button").click()
        
        
})

it('validate Add to cart',function() {
cy.visit("https://demo.nopcommerce.com/")
    cy.wait(5000)
    cy.get(":nth-child(4) > .product-item > .details > .add-info > .buttons > .product-box-add-to-cart-button").click()
    cy.wait(5000)
    cy.get("#product-details-form > div:nth-child(2) > div.product-essential > div.overview > div.overview-buttons > div.email-a-friend > button").click()
    cy.get("#FriendEmail").type('sbshaliniyadav@gmail.com')
    cy.get("#YourEmailAddress").type('shalini.yadav@fiftyfivetech.io')
    cy.get("#PersonalMessage").type('Hello,thankyou for shoping')
    cy.wait(5000)
    cy.get(".buttons > .button-1").click()
    cy.get('.message-error > ul > li').should('contain', 'Only registered customers can use email a friend feature')
    //cy.get('.message-error > ul > li').contains('Only registered customers can use email a friend feature')
    cy.wait(2000)
        
    

})
})
