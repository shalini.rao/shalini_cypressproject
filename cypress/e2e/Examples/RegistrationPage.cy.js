// <reference types="Cypress"/>

import registerpage from '../PageObject/Registerpage.js'

describe('Test Suite',() =>{
    before(function() {
        cy.fixture('example').then(function(data)  {
            this.data=data
        })

    })
        
    

    it('valid Registration',()=>{
        const WebRegisterPage = new registerpage()
        cy.visit('')
        
        WebRegisterPage.getFirstName('Shalini')
        WebRegisterPage.getLastName('Yadav')
        WebRegisterPage.getEmail('shalini.yadav@fiftytech.io')
        
       
        
        WebRegisterPage.getPassword('1234567890')
        WebRegisterPage.getConfirmPassword('1234567890')

        WebRegisterPage.RegisterButton();
        
    })
        


    })

