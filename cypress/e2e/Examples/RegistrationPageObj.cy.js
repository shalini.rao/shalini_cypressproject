/// <reference types="cypress" />

import Registration from "../PageObject/Registration"

describe("Cypress POM Test Suite", function () {
    before(function() {
        cy.fixture('DemoCorpFixturedata').then(function(data)  
        {
            this.data=data
        })

    })


    it("Registration with valid credentials", function () {
        const registrationpage = new Registration();
        registrationpage.open();
        registrationpage.Gender().check()
        registrationpage.FirstName().type(this.data.FirstName)
        registrationpage.LastName().type(this.data.LastName)
        registrationpage.Email().type(this.data.email)
        registrationpage.Company().type(this.data.company)
        registrationpage.Newsletter().check()
        registrationpage.Password().type(this.data.password)
        registrationpage.ConfirmPassword().type(this.data.confirmpassword)
        registrationpage.submit().click()
      
        cy.url("demo.nopcommerce.com").should('include','demo')
    });
});