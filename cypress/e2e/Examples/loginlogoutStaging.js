/// <reference types="Cypress"/>

describe('Atlas Staging login', function()
{

    it('Valid Staging login',function() {


cy.visit("https://login-staging.connectel.io/")

cy.get('input[name=username]').type('sahil.gulati@connectel.se')
cy.wait(5000)
cy.get("#pushConfirmation").click()
cy.on("window:confirm",(str)=>
{expect(str).to.equal("Allow")})
cy.get("input[type=password]").click({force: true}).type("Sahil@123")
cy.get("input[type=checkbox]").uncheck().should('not.be.checked')
cy.wait(1000)

cy.get("#login-form > button").click({force: true})

    } )

})


//Invalid Password

{

    it.only('InValid Password',function() {


cy.visit("https://login-staging.connectel.io/")

cy.get('input[name=username]').type('sahil.gulti@connectel.se')
cy.wait(5000)
cy.get("#pushConfirmation").click({force: true})
cy.on("window:confirm",(str)=>
{expect(str).to.equal("Allow")})
cy.get("input[type=password]").click({force: true}).type("Sail@123")
cy.get("input[type=checkbox]").uncheck().should('not.be.checked')
cy.wait(1000)

cy.get("#login-form > button").click({force: true})

    } )

}

//Verify Go Back

{

    it('Verify Go & Back',function() {
        cy.visit("https://login-staging.connectel.io/")
        cy.title("Connectel").should('eq',"Connectel")
        cy.wait(2000)
        cy.visit("https://status.connectel.io/")
        //cy.title("Google").should('eq','Google')
        cy.wait(2000)
        cy.go('back')
        cy.title('https://login-staging.connectel.io/')
        cy.go('forward')
        cy.wait(2000)
        cy.title("https://status.connectel.io/")
        cy.wait(2000)
        cy.reload()
    })}