export function fillText(element, data) {
  cy.get(element).type(data);
 
  
}
export function RegisterButton(element) {
  cy.get(element).click();
}
export function validateRegisterationpage(element, text) {
  cy.get(element).should("have.text", text);
}
