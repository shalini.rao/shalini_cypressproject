/// <reference types="cypress" />

export function navigate() {
  cy.visit(
    "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login"
  );
}
export function getUsername(username) {
  cy.get("input[placeholder='Username']").type(username);

}
export function getPassword(password) {
  cy.get("input[placeholder='Password']").type(password);
}
export function clickSubmit() {
  cy.get(".oxd-button").click();
}
export function verifyDashboard() {
  cy.get(".oxd-topbar-header-breadcrumb > .oxd-text").should(
    "have.text",
    "Dashboard"
  );
}
export function verifyInvalidCredentialsErrorMessage() {
    cy.get(".oxd-alert-content > .oxd-text").should(
        "have.text",
        "Invalid credentials"
      );
}
