/// <reference types="cypress" />

export function navigateLogin()
{
  cy.visit("https://demo.nopcommerce.com/login?returnUrl=%2F")
}
export function getLoginCredentials(email, password, loginbutton) 
{
  cy.get('[id=Email]').type(email);
  cy.get("[id=Password]").type(password);
  cy.wait(2000);
  cy.get("form > .buttons > .button-1").click();
}

export function verifyLoginDashboard() {
    cy.url().should("eq", "https://demo.nopcommerce.com/");
  }

  export function verifyInvalidEmailErrorMessage() {
    cy.get("#Email-error").should("exist").contains("Wrong email");
  }
  export function verifyInvalidPasswordErrorMessage() {
    cy.get(".message-error")
      .should("exist")
      .contains(
        "Login was unsuccessful. Please correct the errors and try again.The credentials provided are incorrect"
      )
  }

  


