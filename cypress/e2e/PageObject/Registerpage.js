/// <reference types="Cypress"/>

export function visitUrl() {
  cy.visit("https://demo.nopcommerce.com/register?returnUrl=%2F");
}

export function verifyRegistrationFormWithValidDetails(
  gender,
  firstname,
  lastname,
  email,
  company,
  password,
  confirmpassword,
  registerbutton
) {
  cy.fixture("DemoCorpFixturedata.json").then((data) => {
    visitUrl();
    cy.get("#gender-male").check();
    cy.get("#FirstName").type(data.firstname);
    cy.get("#LastName").type(data.lastname);
    cy.get("[id=Email]").type(data.email);
    cy.get("#Company").type(data.company);
    cy.get("[id=Password]").type(data.password);
    cy.get("#ConfirmPassword").type(data.confirmpassword);
    cy.get("#register-button").click();
  });

}
export function verifyIfEmailAlreadyRegistered() {
  cy.get(".message-error > ul > li").should(
    "have.text",
    "The specified email already exists"
  );
}
export function verifyRegistrationFormWithInvalidEmail(
  gender,
  firstname,
  lastname,
  email,
  company
) {
  cy.fixture("DemoCorpFixturedata.json").then((data) => {
    visitUrl();
    cy.get("#gender-male").check();
    cy.get("#FirstName").type(data.firstname);
    cy.get("#LastName").type(data.lastname);
    cy.get("[id=Email]").type(data.invalidemail);
    cy.get("#Company").type(data.company);
  });
}
export function verifyInvalidEmailErrorMessage() {
  cy.get("#Email-error").should("have.text", "Wrong email");
}
export function verifyRegistrationFormWithInvalidPassword(
  gender,
  firstname,
  lastname,
  email,
  company,
  password,
  confirmpassword,
  registerbutton
) {
  cy.fixture("DemoCorpFixturedata.json").then((data) => {
    visitUrl();
    cy.get("#gender-male").check();
    cy.get("#FirstName").type(data.firstname);
    cy.get("#LastName").type(data.lastname);
    cy.get("[id=Email]").type(data.email);
    cy.get("#Company").type(data.company);
    cy.get("[id=Password]").type(data.password);
    cy.get("#ConfirmPassword").type(data.invalidconfirmpassword);
    cy.get("#register-button").click();
  });
}
export function verifyPasswordNotMatchingErrorMessage() {
  cy.get("#ConfirmPassword-error").should(
    "have.text",
    "The password and confirmation password do not match."
  );
}
