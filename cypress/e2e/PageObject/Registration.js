/// <reference types="Cypress"/>
import {
  fillText,
  RegisterButton,
  
} from "./CommonMethods";
const GenderField = "#gender-male";
const FirstnameField = "#FirstName";
const LastnameField = "#LastName";
const EmailField = "[id=Email]";
const CompanyField = "#Company";
const PasswordField = "#Password";
const ConfirmPasswordField = "#ConfirmPassword";
const RegisterationButton = "#register-button";
const SuccessfulRegistrationMessage = ".message-error > ul > li";
const InvalidEmailMessage = "#Email-error";
const InvalidPasswordMessage = "#ConfirmPassword-error";

export function visitUrl() {
  cy.visit("https://demo.nopcommerce.com/register?", {
    failOnStatusCode: false,
  });
}

export function checkGender(element) {
  cy.get(GenderField).check();
}

export function fillFirstName(firstname) {
  fillText(FirstnameField, firstname);
}
export function fillLastName(lastname) {
  fillText(LastnameField, lastname);
}

export function fillEmail(emailaddress) {
  fillText(EmailField, emailaddress);
}
export function fillCompanyname(company) {
  fillText(CompanyField, company);
}
export function fillPassword(password) {
  fillText(PasswordField, password);
}
export function fillConfirmPassword(confirmpassword) {
  fillText(ConfirmPasswordField, confirmpassword);
}
export function clickRegisterationButton() {
  RegisterButton(RegisterationButton);
}
export function validateRegisterationpage(element, text) {
  cy.get(element).should("have.text", text);
}
export function verifyRegistrationFormWithValidDetails() {
  cy.fixture("DemoCorpFixturedata.json").then((data) => {
    checkGender();
    fillFirstName(data.firstname);
    fillLastName(data.lastname);
    fillEmail(data.emailaddress);
    fillCompanyname(data.company);
    fillPassword(data.password);
    fillConfirmPassword(data.confirmpassword);
    clickRegisterationButton();
    validateRegisterationpage(
      SuccessfulRegistrationMessage,
      data.alreadyregisteredemailaddress
    );
  });
}

export function verifyRegistrationFormWithInvalidEmail() {
  cy.fixture("DemoCorpFixturedata.json").then((data) => {
    checkGender();
    fillFirstName(data.firstname);
    fillLastName(data.lastname);
    fillEmail(data.invalidemailaddress);
    fillCompanyname(data.company);
    validateRegisterationpage(InvalidEmailMessage, data.invalidEmailErrorText);
  });
}

export function verifyRegistrationFormWithInvalidPassword() {
  cy.fixture("DemoCorpFixturedata.json").then((data) => {
    checkGender();
    fillFirstName(data.firstname,);
    fillLastName(data.lastname);
    fillEmail(data.emailaddress);
    fillCompanyname(data.company);
    fillPassword(data.password);
    fillConfirmPassword(data.invalidconfirmpassword);
    clickRegisterationButton();
    validateRegisterationpage(
      InvalidPasswordMessage,
      data.invalidPasswordErrorText
    );
  });
}

