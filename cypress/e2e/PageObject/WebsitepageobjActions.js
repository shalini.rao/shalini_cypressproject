class LoginDummyWebsite {
  WebsiteUrl =
    "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";
  UsernameField = 'input[placeholder="Username"]';
  PasswordField = 'input[placeholder="Password"]';
  LoginButton = 'button[type="Submit"]';
  TargetUrl =
    "https://opensource-demo.orangehrmlive.com/web/index.php/dashboard/index";
  UserProfileDropdown = ".oxd-userdropdown-tab > .oxd-icon";
  LogoutIcon = ":nth-child(4) > .oxd-userdropdown-link";
  ErrorText = ".oxd-alert-content > .oxd-text";

  getUrl() {
    cy.visit(this.WebsiteUrl);
  }

  loginUsernameAndPassword(username, password) {
    cy.get(this.UsernameField).type(username);
    cy.get(this.PasswordField).type(password);
    cy.get(this.LoginButton).click();
  }
  verifyErrorMessageForInvalidCredentials() {
    cy.get(this.ErrorText).should("exist").contains("Invalid credentials");
  }

  verifyDashboard() {
    cy.url().should("eq", this.TargetUrl);
  }

  clickLogOutIcon() {
    cy.get(this.UserProfileDropdown).click();
    cy.wait(3000);
    cy.get(this.LogoutIcon).click();
  }
}

  
export default LoginDummyWebsite;
