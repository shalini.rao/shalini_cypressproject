
import { click, fillText } from "../PageObject/WebsitepageobjActions";
//import const fillUseremail,fillUserPassword from "../PageObject/Registration";

const userEmail = "[id=Email]";
const userPassword = "[id=Password]";
const urlLogin = "https://demo.nopcommerce.com/login?returnUrl=%2F";
const logIn = "form > .buttons > .button-1";
const logout = ".ico-logout";
const emailErrorText = "#Email-error";
const passwordErrorText = ".message-error";
const targetUrl = "https://demo.nopcommerce.com/";



export const loginWebsiteUrl = () => {
  
  cy.visit(urlLogin);
};
export const fillUseremail = (userEmail) => {
  fillText(emailField, userEmail);
};
export const fillUserPassword = (userPassword) => {
  fillText(passwordField, userPassword);
};
export const loginButton = () => {
  click();
};
export const logoutButton = () => {
  click();
};
export const invalidEmailAlertMessage = () => {
  cy.get(this.emailErrorText).should("exist").contains("Wrong email");
}
export const invalidPasswordAlertMessage = () => {
  cy.get(this.passwordErrorText)
      .should("exist")
      .contains(
        "Login was unsuccessful. Please correct the errors and try again.The credentials provided are incorrect"
      )}
      export const verifydashboard = () => {
        cy.url().should("eq", this.targetUrl);
      }
export const UserLogin = () => {
  loginWebsiteUrl();
  cy.fixture('loginCredentials/DemoCorpFixturedata').then((data) => {
    fillUseremail(data.validUserAdmin);
    fillUserPassword(data.validPassword);
    invalidEmailAlertMessage();
    invalidPasswordAlertMessage();
    verifydashboard();
    loginButton();

  });
  
  cy.wait(2000);
  logoutButton();
};